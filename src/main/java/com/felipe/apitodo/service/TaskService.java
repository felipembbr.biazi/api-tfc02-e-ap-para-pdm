package com.felipe.apitodo.service;

import com.felipe.apitodo.model.TaskModel;
import com.felipe.apitodo.repository.ApiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {
    @Autowired
    private ApiRepository apiRepository;

    public TaskModel findById(Long id){
        return apiRepository.findById(id).orElseThrow(() -> new RuntimeException());
    }
    public TaskModel updateTask(Long id, Boolean isDone){
        TaskModel task = apiRepository.findById(id).orElseThrow(() -> new RuntimeException());
        task.setIsDone(isDone);
         return apiRepository.save(task);
    }
    public List<TaskModel> findAll (){
        return apiRepository.findAll();
    }

    public TaskModel saveTask (TaskModel taskModel){
        return apiRepository.save(taskModel);
    }

    public String delTask(Long id){
        if(apiRepository.existsById(id)){
            apiRepository.deleteById(id);
            return "Removido com Sucesso!";
        }else{
            return "Não foi possivel encontrar a task!";
        }
    }
}

