package com.felipe.apitodo.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TaskTodo")
@Getter
@Setter
@EqualsAndHashCode
public class TaskModel implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String txt_task;
    private Boolean isUrgent;
    private Boolean  isDone;
}
