package com.felipe.apitodo.repository;

import com.felipe.apitodo.model.TaskModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApiRepository extends JpaRepository<TaskModel,Long> {
}
