package com.felipe.apitodo.controller;

import com.felipe.apitodo.model.TaskModel;
import com.felipe.apitodo.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/listAll")
    public ResponseEntity<List<TaskModel>> listTask(){
        return new ResponseEntity<>(taskService.findAll(),HttpStatus.OK);
    }

    @PostMapping("/new")
    @ResponseBody
    public ResponseEntity<TaskModel> saveTask(@RequestBody TaskModel taskModel){
        return new ResponseEntity<>(taskService.saveTask(taskModel), HttpStatus.CREATED);
    }

    @PutMapping("/done/{isDone}/{id}")
    @ResponseBody
    public ResponseEntity<TaskModel> updateTask(@PathVariable(name ="isDone")Boolean isDone, @PathVariable(name ="id")Long id){
        return new ResponseEntity<>(taskService.updateTask(id, isDone),HttpStatus.OK);
    }

    @DeleteMapping("/del/{id}")
    public ResponseEntity<String> dellTask(@PathVariable(name = "id")Long id){
        return new ResponseEntity<>(taskService.delTask(id),HttpStatus.OK);
    }

}
